<?php
namespace AdminModules\Custom\NovumCjib\Vorderingen\Vordering\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumCjib\Vordering\CrudVorderingManager;
use Crud\FormManager;
use Model\Custom\NovumCjib\Vordering;
use Model\Custom\NovumCjib\VorderingQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumCjib\Vorderingen\Vordering instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Vorderingen";
	}


	public function getModule(): string
	{
		return "Vordering";
	}


	public function getManager(): FormManager
	{
		return new CrudVorderingManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return VorderingQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Vordering){
		    LogActivity::register("Vorderingen", "Vorderingen verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Vorderingen verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Vorderingen niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Vorderingen item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
