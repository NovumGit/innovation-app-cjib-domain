<?php
namespace AdminModules\Custom\NovumCjib\Vorderingen\Vordering\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumCjib\Vordering\CrudVorderingManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumCjib\Vorderingen\Vordering instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudVorderingManager();
	}


	public function getPageTitle(): string
	{
		return "Vorderingen";
	}
}
