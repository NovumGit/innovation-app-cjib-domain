<?php

if(isset($_SERVER['IS_DEVEL']))
{
    $aConfig = [
        'PROTOCOL' => 'http',
        'ADMIN_PROTOCOL' => 'http',
        'CUSTOM_FOLDER' => 'NovumCjib',
        'ABSOLUTE_ROOT' => '/home/anton/Documents/sites/hurah',
        'DOMAIN' => 'cjib.demo.novum.nuidev.nl',
        /* Je zoekt waarschijnlijk Config::getDataDir() */
        'DATA_DIR' => '../'
    ];
}
else
{
    $aConfig = [
        'PROTOCOL' => 'https',
        'ADMIN_PROTOCOL' => 'https',
        'CUSTOM_FOLDER' => 'NovumCjib',
        'DOMAIN' => 'cjib.demo.novum.nu',
        /* Je zoekt waarschijnlijk Config::getDataDir() */
        'ABSOLUTE_ROOT' => '/home/nov_cjib/platform/system',
        'DATA_DIR' => '/home/nov_cjib/platform/data'
    ];
}

$aConfig['CUSTOM_NAMESPACE'] = 'NovumCjib';
return $aConfig;

