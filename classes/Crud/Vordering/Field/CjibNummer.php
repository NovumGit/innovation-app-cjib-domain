<?php
namespace Crud\Custom\NovumCjib\Vordering\Field;

use Crud\Custom\NovumCjib\Vordering\Field\Base\CjibNummer as BaseCjibNummer;

/**
 * Skeleton subclass for representing cjib_nummer field from the vordering table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CjibNummer extends BaseCjibNummer
{
}
