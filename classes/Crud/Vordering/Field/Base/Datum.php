<?php
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Custom\NovumCjib\Vordering\CrudFieldIterator;
use Crud\Generic\Field\GenericDateTime;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'datum' crud field from the 'vordering' table.
 * This class is auto generated and should not be modified.
 */
abstract class Datum extends GenericDateTime implements IFilterableField, IEditableField
{
	protected $sFieldName = 'datum';
	protected $sFieldLabel = 'Datum';
	protected $sIcon = 'calendar';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDatum';
	protected $sFqModelClassname = '\Model\Custom\NovumCjib\Vordering';


	public function isUniqueKey(): bool
	{
		return false;
	}
}
