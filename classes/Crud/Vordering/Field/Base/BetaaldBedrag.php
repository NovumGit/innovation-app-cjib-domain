<?php
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Custom\NovumCjib\Vordering\CrudFieldIterator;
use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'betaald_bedrag' crud field from the 'vordering' table.
 * This class is auto generated and should not be modified.
 */
abstract class BetaaldBedrag extends GenericMoney implements IFilterableField, IEditableField
{
	protected $sFieldName = 'betaald_bedrag';
	protected $sFieldLabel = 'Betaald bedrag';
	protected $sIcon = 'money';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getBetaaldBedrag';
	protected $sFqModelClassname = '\Model\Custom\NovumCjib\Vordering';


	public function isUniqueKey(): bool
	{
		return false;
	}
}
