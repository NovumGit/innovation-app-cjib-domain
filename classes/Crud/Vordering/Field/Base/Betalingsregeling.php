<?php
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Custom\NovumCjib\Vordering\CrudFieldIterator;
use Crud\Generic\Field\GenericBoolean;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'betalingsregeling' crud field from the 'vordering' table.
 * This class is auto generated and should not be modified.
 */
abstract class Betalingsregeling extends GenericBoolean implements IFilterableField, IEditableField
{
	protected $sFieldName = 'betalingsregeling';
	protected $sFieldLabel = 'Betalingsregeling';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getBetalingsregeling';
	protected $sFqModelClassname = '\Model\Custom\NovumCjib\Vordering';


	public function isUniqueKey(): bool
	{
		return false;
	}
}
