<?php
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Custom\NovumCjib\Vordering\CrudFieldIterator;
use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'origineel_bedrag' crud field from the 'vordering' table.
 * This class is auto generated and should not be modified.
 */
abstract class OrigineelBedrag extends GenericMoney implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'origineel_bedrag';
	protected $sFieldLabel = 'Origineel bedrag';
	protected $sIcon = 'money';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getOrigineelBedrag';
	protected $sFqModelClassname = '\Model\Custom\NovumCjib\Vordering';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['origineel_bedrag']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Origineel bedrag" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
