<?php
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Custom\NovumCjib\Vordering\CrudFieldIterator;
use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'cjib_nummer' crud field from the 'vordering' table.
 * This class is auto generated and should not be modified.
 */
abstract class CjibNummer extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'cjib_nummer';
	protected $sFieldLabel = 'CJIB nummer';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCjibNummer';
	protected $sFqModelClassname = '\Model\Custom\NovumCjib\Vordering';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['cjib_nummer']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "CJIB nummer" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
