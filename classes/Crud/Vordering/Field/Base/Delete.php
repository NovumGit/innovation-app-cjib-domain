<?php 
namespace Crud\Custom\NovumCjib\Vordering\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCjib\Vordering;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Vordering)
		{
		     return "/custom/novumcjib/vorderingen/vordering/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Vordering)
		{
		     return "/custom/novumcjib/vordering?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
