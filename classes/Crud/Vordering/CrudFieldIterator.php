<?php
namespace Crud\Custom\NovumCjib\Vordering;

use Crud\Custom\NovumCjib\Vordering\Base\CrudFieldIterator as BaseIterator;

/**
 * Skeleton crud field iterator for representing a collection of Vordering crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class CrudFieldIterator extends \CrudFieldIterator
{
}
