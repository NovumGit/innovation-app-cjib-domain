<?php
namespace Crud\Custom\NovumCjib\Vordering\Base;

use Core\Utils;
use Crud\Custom\NovumCjib;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCjib\Map\VorderingTableMap;
use Model\Custom\NovumCjib\Vordering;
use Model\Custom\NovumCjib\VorderingQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Vordering instead if you need to override or add functionality.
 */
abstract class CrudVorderingManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCjib\CrudTrait;
	use NovumCjib\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return VorderingQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCjib\Map\VorderingTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn boetes / vorderingen opgenomen";
	}


	public function getEntityTitle(): string
	{
		return "Vordering";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumcjib/vorderingen/vordering/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumcjib/vorderingen/vordering/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Vorderingen toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Vorderingen aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PersoonId', 'CjibNummer', 'OrigineelBedrag', 'BetaaldBedrag', 'Datum', 'Betalingsregeling', 'Delete', 'Edit'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PersoonId', 'CjibNummer', 'OrigineelBedrag', 'BetaaldBedrag', 'Datum', 'Betalingsregeling', 'Delete', 'Edit'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Vordering
	 */
	public function getModel(array $aData = null): Vordering
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oVorderingQuery = VorderingQuery::create();
		     $oVordering = $oVorderingQuery->findOneById($aData['id']);
		     if (!$oVordering instanceof Vordering) {
		         throw new LogicException("Vordering should be an instance of Vordering but got something else." . __METHOD__);
		     }
		     $oVordering = $this->fillVo($aData, $oVordering);
		}
		else {
		     $oVordering = new Vordering();
		     if (!empty($aData)) {
		         $oVordering = $this->fillVo($aData, $oVordering);
		     }
		}
		return $oVordering;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Vordering
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Vordering
	{
		$oVordering = $this->getModel($aData);


		 if(!empty($oVordering))
		 {
		     $oVordering = $this->fillVo($aData, $oVordering);
		     $oVordering->save();
		 }
		return $oVordering;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Vordering $oModel
	 * @return Vordering
	 */
	protected function fillVo(array $aData, Vordering $oModel): Vordering
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['cjib_nummer']) ? $oModel->setCjibNummer($aData['cjib_nummer']) : null;
		isset($aData['origineel_bedrag']) ? $oModel->setOrigineelBedrag($aData['origineel_bedrag']) : null;
		isset($aData['betaald_bedrag']) ? $oModel->setBetaaldBedrag($aData['betaald_bedrag']) : null;
		isset($aData['datum']) ? $oModel->setDatum($aData['datum']) : null;
		isset($aData['betalingsregeling']) ? $oModel->setBetalingsregeling($aData['betalingsregeling']) : null;
		return $oModel;
	}
}
