<?php
namespace Crud\Custom\NovumCjib\Persoon\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Custom\NovumCjib\Persoon\ICrudCollectionField instead if you need to override or add functionality.
 */
interface ICrudCollectionField extends IField
{
}
