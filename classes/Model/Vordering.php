<?php

namespace Model\Custom\NovumCjib;

use Model\Custom\NovumCjib\Base\Vordering as BaseVordering;

/**
 * Skeleton subclass for representing a row from the 'vordering' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Vordering extends BaseVordering
{

}
