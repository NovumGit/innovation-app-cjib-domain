<?php

namespace Model\Custom\NovumCjib\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumCjib\Vordering as ChildVordering;
use Model\Custom\NovumCjib\VorderingQuery as ChildVorderingQuery;
use Model\Custom\NovumCjib\Map\VorderingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vordering' table.
 *
 *
 *
 * @method     ChildVorderingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildVorderingQuery orderByPersoonId($order = Criteria::ASC) Order by the persoon_id column
 * @method     ChildVorderingQuery orderByCjibNummer($order = Criteria::ASC) Order by the cjib_nummer column
 * @method     ChildVorderingQuery orderByOrigineelBedrag($order = Criteria::ASC) Order by the origineel_bedrag column
 * @method     ChildVorderingQuery orderByBetaaldBedrag($order = Criteria::ASC) Order by the betaald_bedrag column
 * @method     ChildVorderingQuery orderByDatum($order = Criteria::ASC) Order by the datum column
 * @method     ChildVorderingQuery orderByBetalingsregeling($order = Criteria::ASC) Order by the betalingsregeling column
 *
 * @method     ChildVorderingQuery groupById() Group by the id column
 * @method     ChildVorderingQuery groupByPersoonId() Group by the persoon_id column
 * @method     ChildVorderingQuery groupByCjibNummer() Group by the cjib_nummer column
 * @method     ChildVorderingQuery groupByOrigineelBedrag() Group by the origineel_bedrag column
 * @method     ChildVorderingQuery groupByBetaaldBedrag() Group by the betaald_bedrag column
 * @method     ChildVorderingQuery groupByDatum() Group by the datum column
 * @method     ChildVorderingQuery groupByBetalingsregeling() Group by the betalingsregeling column
 *
 * @method     ChildVorderingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVorderingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVorderingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVorderingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVorderingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVorderingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVorderingQuery leftJoinPersoon($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persoon relation
 * @method     ChildVorderingQuery rightJoinPersoon($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persoon relation
 * @method     ChildVorderingQuery innerJoinPersoon($relationAlias = null) Adds a INNER JOIN clause to the query using the Persoon relation
 *
 * @method     ChildVorderingQuery joinWithPersoon($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Persoon relation
 *
 * @method     ChildVorderingQuery leftJoinWithPersoon() Adds a LEFT JOIN clause and with to the query using the Persoon relation
 * @method     ChildVorderingQuery rightJoinWithPersoon() Adds a RIGHT JOIN clause and with to the query using the Persoon relation
 * @method     ChildVorderingQuery innerJoinWithPersoon() Adds a INNER JOIN clause and with to the query using the Persoon relation
 *
 * @method     \Model\Custom\NovumCjib\PersoonQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVordering findOne(ConnectionInterface $con = null) Return the first ChildVordering matching the query
 * @method     ChildVordering findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVordering matching the query, or a new ChildVordering object populated from the query conditions when no match is found
 *
 * @method     ChildVordering findOneById(int $id) Return the first ChildVordering filtered by the id column
 * @method     ChildVordering findOneByPersoonId(int $persoon_id) Return the first ChildVordering filtered by the persoon_id column
 * @method     ChildVordering findOneByCjibNummer(string $cjib_nummer) Return the first ChildVordering filtered by the cjib_nummer column
 * @method     ChildVordering findOneByOrigineelBedrag(string $origineel_bedrag) Return the first ChildVordering filtered by the origineel_bedrag column
 * @method     ChildVordering findOneByBetaaldBedrag(string $betaald_bedrag) Return the first ChildVordering filtered by the betaald_bedrag column
 * @method     ChildVordering findOneByDatum(string $datum) Return the first ChildVordering filtered by the datum column
 * @method     ChildVordering findOneByBetalingsregeling(boolean $betalingsregeling) Return the first ChildVordering filtered by the betalingsregeling column *

 * @method     ChildVordering requirePk($key, ConnectionInterface $con = null) Return the ChildVordering by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOne(ConnectionInterface $con = null) Return the first ChildVordering matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVordering requireOneById(int $id) Return the first ChildVordering filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByPersoonId(int $persoon_id) Return the first ChildVordering filtered by the persoon_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByCjibNummer(string $cjib_nummer) Return the first ChildVordering filtered by the cjib_nummer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByOrigineelBedrag(string $origineel_bedrag) Return the first ChildVordering filtered by the origineel_bedrag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByBetaaldBedrag(string $betaald_bedrag) Return the first ChildVordering filtered by the betaald_bedrag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByDatum(string $datum) Return the first ChildVordering filtered by the datum column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVordering requireOneByBetalingsregeling(boolean $betalingsregeling) Return the first ChildVordering filtered by the betalingsregeling column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVordering[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVordering objects based on current ModelCriteria
 * @method     ChildVordering[]|ObjectCollection findById(int $id) Return ChildVordering objects filtered by the id column
 * @method     ChildVordering[]|ObjectCollection findByPersoonId(int $persoon_id) Return ChildVordering objects filtered by the persoon_id column
 * @method     ChildVordering[]|ObjectCollection findByCjibNummer(string $cjib_nummer) Return ChildVordering objects filtered by the cjib_nummer column
 * @method     ChildVordering[]|ObjectCollection findByOrigineelBedrag(string $origineel_bedrag) Return ChildVordering objects filtered by the origineel_bedrag column
 * @method     ChildVordering[]|ObjectCollection findByBetaaldBedrag(string $betaald_bedrag) Return ChildVordering objects filtered by the betaald_bedrag column
 * @method     ChildVordering[]|ObjectCollection findByDatum(string $datum) Return ChildVordering objects filtered by the datum column
 * @method     ChildVordering[]|ObjectCollection findByBetalingsregeling(boolean $betalingsregeling) Return ChildVordering objects filtered by the betalingsregeling column
 * @method     ChildVordering[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VorderingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Custom\NovumCjib\Base\VorderingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Custom\\NovumCjib\\Vordering', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVorderingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVorderingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVorderingQuery) {
            return $criteria;
        }
        $query = new ChildVorderingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVordering|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VorderingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VorderingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVordering A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, persoon_id, cjib_nummer, origineel_bedrag, betaald_bedrag, datum, betalingsregeling FROM vordering WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVordering $obj */
            $obj = new ChildVordering();
            $obj->hydrate($row);
            VorderingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVordering|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(VorderingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(VorderingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(VorderingTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(VorderingTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the persoon_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPersoonId(1234); // WHERE persoon_id = 1234
     * $query->filterByPersoonId(array(12, 34)); // WHERE persoon_id IN (12, 34)
     * $query->filterByPersoonId(array('min' => 12)); // WHERE persoon_id > 12
     * </code>
     *
     * @see       filterByPersoon()
     *
     * @param     mixed $persoonId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByPersoonId($persoonId = null, $comparison = null)
    {
        if (is_array($persoonId)) {
            $useMinMax = false;
            if (isset($persoonId['min'])) {
                $this->addUsingAlias(VorderingTableMap::COL_PERSOON_ID, $persoonId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($persoonId['max'])) {
                $this->addUsingAlias(VorderingTableMap::COL_PERSOON_ID, $persoonId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_PERSOON_ID, $persoonId, $comparison);
    }

    /**
     * Filter the query on the cjib_nummer column
     *
     * Example usage:
     * <code>
     * $query->filterByCjibNummer('fooValue');   // WHERE cjib_nummer = 'fooValue'
     * $query->filterByCjibNummer('%fooValue%', Criteria::LIKE); // WHERE cjib_nummer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cjibNummer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByCjibNummer($cjibNummer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cjibNummer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_CJIB_NUMMER, $cjibNummer, $comparison);
    }

    /**
     * Filter the query on the origineel_bedrag column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigineelBedrag('fooValue');   // WHERE origineel_bedrag = 'fooValue'
     * $query->filterByOrigineelBedrag('%fooValue%', Criteria::LIKE); // WHERE origineel_bedrag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $origineelBedrag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByOrigineelBedrag($origineelBedrag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($origineelBedrag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_ORIGINEEL_BEDRAG, $origineelBedrag, $comparison);
    }

    /**
     * Filter the query on the betaald_bedrag column
     *
     * Example usage:
     * <code>
     * $query->filterByBetaaldBedrag('fooValue');   // WHERE betaald_bedrag = 'fooValue'
     * $query->filterByBetaaldBedrag('%fooValue%', Criteria::LIKE); // WHERE betaald_bedrag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $betaaldBedrag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByBetaaldBedrag($betaaldBedrag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($betaaldBedrag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_BETAALD_BEDRAG, $betaaldBedrag, $comparison);
    }

    /**
     * Filter the query on the datum column
     *
     * Example usage:
     * <code>
     * $query->filterByDatum('2011-03-14'); // WHERE datum = '2011-03-14'
     * $query->filterByDatum('now'); // WHERE datum = '2011-03-14'
     * $query->filterByDatum(array('max' => 'yesterday')); // WHERE datum > '2011-03-13'
     * </code>
     *
     * @param     mixed $datum The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByDatum($datum = null, $comparison = null)
    {
        if (is_array($datum)) {
            $useMinMax = false;
            if (isset($datum['min'])) {
                $this->addUsingAlias(VorderingTableMap::COL_DATUM, $datum['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datum['max'])) {
                $this->addUsingAlias(VorderingTableMap::COL_DATUM, $datum['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VorderingTableMap::COL_DATUM, $datum, $comparison);
    }

    /**
     * Filter the query on the betalingsregeling column
     *
     * Example usage:
     * <code>
     * $query->filterByBetalingsregeling(true); // WHERE betalingsregeling = true
     * $query->filterByBetalingsregeling('yes'); // WHERE betalingsregeling = true
     * </code>
     *
     * @param     boolean|string $betalingsregeling The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByBetalingsregeling($betalingsregeling = null, $comparison = null)
    {
        if (is_string($betalingsregeling)) {
            $betalingsregeling = in_array(strtolower($betalingsregeling), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(VorderingTableMap::COL_BETALINGSREGELING, $betalingsregeling, $comparison);
    }

    /**
     * Filter the query by a related \Model\Custom\NovumCjib\Persoon object
     *
     * @param \Model\Custom\NovumCjib\Persoon|ObjectCollection $persoon The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVorderingQuery The current query, for fluid interface
     */
    public function filterByPersoon($persoon, $comparison = null)
    {
        if ($persoon instanceof \Model\Custom\NovumCjib\Persoon) {
            return $this
                ->addUsingAlias(VorderingTableMap::COL_PERSOON_ID, $persoon->getId(), $comparison);
        } elseif ($persoon instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VorderingTableMap::COL_PERSOON_ID, $persoon->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPersoon() only accepts arguments of type \Model\Custom\NovumCjib\Persoon or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persoon relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function joinPersoon($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persoon');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persoon');
        }

        return $this;
    }

    /**
     * Use the Persoon relation Persoon object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumCjib\PersoonQuery A secondary query class using the current class as primary query
     */
    public function usePersoonQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersoon($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persoon', '\Model\Custom\NovumCjib\PersoonQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVordering $vordering Object to remove from the list of results
     *
     * @return $this|ChildVorderingQuery The current query, for fluid interface
     */
    public function prune($vordering = null)
    {
        if ($vordering) {
            $this->addUsingAlias(VorderingTableMap::COL_ID, $vordering->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vordering table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VorderingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VorderingTableMap::clearInstancePool();
            VorderingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VorderingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VorderingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VorderingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VorderingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VorderingQuery
